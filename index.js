const express = require("express");
const mongoose = require("mongoose");

const app = express();

mongoose.connect("mongodb://localhost:27017/bookstore");

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

require('./models/dbs.js');
require('./route/routes.js')(app);

app.listen(3000, () => {
      console.log("Listening at : http://localhost:3000");
    });