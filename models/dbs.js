const mongoose = require("mongoose");
    
mongoose.connect("mongodb://localhost:27017/bookstore",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const BookModel = mongoose.model("books",{
    _id: {
        type: Number,
        required: [true, "Id required!"]
    },
    title: {
        type: String,
        required: [true, "Title required!"]
    },
    author: {
        type: Array,
        required: [true, "Author required!"]
    },
    published_date: {
        type: Date,
        required: [true, "Publish Date required!"]
    },
    pages: {
        type: Number,
        required: [true, "Pages required!"]
    },
    language: {
        type: String,
        required: [true, "Language required!"]
    },
    publisher_id: {
        type: String,
        required: [true, "Publisher required!"]
    }
})

exports.model = BookModel;